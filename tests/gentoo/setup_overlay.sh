#!/bin/sh
set -e
mkdir -p /usr/local/portage-crossdev/{profiles,metadata} 
echo 'crossdev' > /usr/local/portage-crossdev/profiles/repo_name 
echo 'masters = gentoo' > /usr/local/portage-crossdev/metadata/layout.conf 
chown -R portage:portage /usr/local/portage-crossdev

