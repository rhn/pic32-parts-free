#ifndef __DELAY_C
#define __DELAY_C
#include <system.c>

void Delayms(uint32_t delay) {
    uint32_t freq = GetSystemClock();
    uint32_t end = GetCP0Count() + delay * freq / 500;
    while (end > GetCP0Count()) {}
}

#endif
