PIC32MM GPL example project
===========================

This is an example program for the PIC32MM Curiosity Development board
[DM320101](https://www.microchip.com/DevelopmentTools/ProductDetails/DM320101), that has a PIC32MM0064GPL036 on it.

## Program overview
The example prints "Hello" Onto UART pins RB14 (RX) and RB15 (TX).
When it receives a character on its RX pin (via an interrupt),
it advances the color of the RGB LED. 

## Programmer connection
The on-board Pickit3 programmer is not used, as it is not supported by the Microchip's scripting firmware, and as such doesn't work with pic32prog.

Instead, the following pins (besides MCLR) are used for programming:
* Pins RB10 and RB11 are used for ICSP - PGED2 and PGEC2 respectively.
* Pins RB8, RB9, RB10, and RB11 are used for JTAG - TCK, TMS, TDO, TSI respectively.

Note, if you wish to completely disable the on-board Pickit3, you can solder a jumper
from its MCLR to GND on headers J1 or J7.
