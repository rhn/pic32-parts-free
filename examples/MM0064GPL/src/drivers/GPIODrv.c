#include <p32xxxx.h>
#include <GPIODrv.h>
#include <inttypes.h>

void GPIODrv_Init(){
	// Setup LEDs, buttons

	// LEDs
	LEDRED_TRISbits.LEDRED_TRISPIN = 0;			// Output
	LEDRED_LATbits.LEDRED_LATPIN = 0;			// Turn LED off

	LEDGREEN_ANSELbits.LEDGREEN_ANSELPIN = 0;	// Disable analog function
	LEDGREEN_TRISbits.LEDGREEN_TRISPIN = 0;		// Output
	LEDGREEN_LATbits.LEDGREEN_LATPIN = 0;		// Turn LED off

	LEDBLUE_TRISbits.LEDBLUE_TRISPIN = 0;		// Output
	LEDBLUE_LATbits.LEDBLUE_LATPIN = 0;			// Turn LED off


	// Buttons
	BTN1_TRISbits.BTN1_TRISPIN = 1;			// Input
	BTN1_PULLREGbits.BTN1_PULLBIT = 0;		// Disable pull-up, because there is an external one present

	BTN2_TRISbits.BTN2_TRISPIN = 1;			// Input
	BTN2_PULLREGbits.BTN2_PULLBIT = 0;		// Disable pull-up, because there is an external one present

}


void LEDred_setState(uint32_t state){
	LEDRED_LATbits.LEDRED_LATPIN = state?1:0;
}

void LEDgreen_setState(uint32_t state){
	LEDGREEN_LATbits.LEDGREEN_LATPIN = state?1:0;
}

void LEDblue_setState(uint32_t state){
	LEDBLUE_LATbits.LEDBLUE_LATPIN = state?1:0;
}

