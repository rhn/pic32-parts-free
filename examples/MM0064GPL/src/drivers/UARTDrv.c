#include <p32xxxx.h>	// Include isrwrapper.h in this
#include <UARTDrv.h>
#include <GPIODrv.h>
//#include <system.h>
#include <inttypes.h>
#include <interrupt.h>

volatile uint32_t count = 0;
uint8_t temp;


// As defined in ISRwrapper.h/c
INTERRUPT(UART2RxInterrupt){
	// Should check if TX or RX interrupt
	count++;		// Increase counter, for our test
	temp = U2RXREG;	// Readout data, otherwise we'll be stuck here
	IFS1bits.U2RXIF = 0;	// Clear bits. TODO nicer.
}


void UARTDrv_Init(uint32_t baud){
	U2MODEbits.ACTIVE = 0;
	U2MODEbits.ON = 0;

	UART_TX_ANSELbits.UART_TX_ANSELPIN = 0;							// Disable analog function ><
	UART_TX_TRISbits.UART_TX_TRISPIN = 0;							// 0 == output
	UART_TX_LATbits.UART_TX_LATPIN = 1;								// Set high, as UART is Idle High
	UART_TX_MAP_REGbits.UART_TX_MAP_REGPERIPH = UART_TX_MAP_REGVAL;	// Remap pin to proper function

	UART_RX_ANSELbits.UART_RX_ANSELPIN = 0;							// Disable analog function ><
	UART_RX_TRISbits.UART_RX_TRISPIN = 1;							// 1 == input
	UART_RX_PULLREGbits.UART_RX_PULLBIT = 1;						// Enable pull-up
	UART_RX_MAP_REGbits.UART_RX_MAP_REGPERIPH = UART_RX_MAP_REGVAL;	// Set to which pin

	U2MODEbits.SLPEN = 1;		// UART clock runs during sleep
	U2MODEbits.CLKSEL = 0b00;	// UART clock is PBCLK (1:1 with SYSCLK)
	U2MODEbits.OVFDIS = 1;		// During overflow error, remain synchronized (new way)
	U2MODEbits.SIDL = 0;		// Continue operation in IDLE mode
	U2MODEbits.IREN = 0;		// IrDA is disabled
	U2MODEbits.RTSMD = 0;		// UxRTS pin is in flow control mode (signals ready to receive data)
	U2MODEbits.UEN = 0b00;		// UxTX and uxRX pins are enabled and used, CTS and RTS controlled by port function 
	U2MODEbits.WAKE = 0;		// Wake-up disabled
	U2MODEbits.LPBACK = 0;		// Loopback mode disabled
	U2MODEbits.ABAUD = 0;		// Autobauding disabled (character 0x55, if enabled btw)
	U2MODEbits.RXINV = 0;		// Idle high state on RX
	U2MODEbits.BRGH = 0;		// Standard speed mode - 16x baud clock
	U2MODEbits.PDSEL = 0b00;	// 8-bit data, no parity
	U2MODEbits.STSEL = 0;		// 1 stop bit

	

	U2STAbits.MASK = 0;		// Don't care for address match mask, unused
	U2STAbits.ADDR = 0;		// Don't care for auto address mark
	U2STAbits.UTXISEL = 00;	// Generate interrupt, when at least one space available (unused)
	U2STAbits.UTXINV = 0;	// Idle HIGH
	U2STAbits.URXEN = 1;	// UART receiver pin enabled
	U2STAbits.UTXBRK = 0;	// Don't send breaks.
	U2STAbits.UTXEN = 1;	// UART transmitter pin enabled
	U2STAbits.URXISEL = 0;	// Interrupt what receiver buffer not empty
	U2STAbits.ADDEN = 0;	// Address detect mode disabled (unused)
	U2STAbits.OERR = 0;		// Clear RX Overrun bit - not important at this point

	// (PBCLK/BRGH?4:16)/BAUD - 1
	//U2BRG = (GetPeripheralClock() / (U2MODEbits.BRGH ? 4 : 16)) / baud - 1;
	U2BRG = (24000000 / (U2MODEbits.BRGH ? 4 : 16)) / baud - 1;
	

	// New - setup interrupt
	//IPC9bits.U2IP = 1;		// Priorty = 1 (one above disabled)
	//IPC9bits.U2IS = 0;		// Subpriority = 0 (least)
	//IEC1bits.U2RXIE = 1;	// Enable interrupt

	IFS1bits.U2RXIF = 0;		// Clear flag
	IEC1bits.U2RXIE = 1;		// Enable interrupt for uart
	IPC10bits.U2RXIP = 5;	// Set interrupt level

	U2MODEbits.ON = 1;		// UART is enabled, pins controlled by UART as defined by UEN and UTXEN
	U2MODEbits.ACTIVE = 1;	// UART is active, U2MODE register shouldn't be updated
}

void UARTDrv_SendBlocking(uint8_t * buffer, uint32_t length){

	uint32_t counter = 0;

	for (counter = 0; counter<length; counter++){
		while(U2STAbits.UTXBF){ asm("nop"); }
		U2TXREG = buffer[counter];
	}

	// Wait until sent
	while(U2STAbits.TRMT){
		_nop();
	}
}

uint32_t UARTDrv_GetCount(){
	return count;
}

