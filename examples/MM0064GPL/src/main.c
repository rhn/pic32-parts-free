#include <p32xxxx.h>    // Always in first place to avoid conflict with const.h -> this includes xc.h
//#include <system.h>     // System setup
#include <const.h>      // MIPS32
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <newlib.h>
#include <errno.h>

#include <GPIODrv.h>
#include <UARTDrv.h>


// Configuration bits for the PIC32MM chip. PRIMARY configuration
const uint32_t __attribute__((section (".SECTION_FDEVOPT"))) temp_fdevopt = 
	0x0000FFF7
	| ((0xF0C5<<1) << _FDEVOPT_USERID_POSITION)	// UserID is F0C5. NOTE! for some reason, it's declared as _17_ bits wide in the .h file. 
												// Hence this hack, to have it aligned, since the definition shifts by 15, instead of 16...
	| (0b1 << _FDEVOPT_SOSCHP_POSITION);	// SOSC operated in Normal Power mode

const uint32_t __attribute__((section (".SECTION_FICD"))) temp_ficd =
	0xFFFFFFE3
	| (0b01 << _FICD_ICS_POSITION)			// Communication is on PGEC3/PGED3
	| (0b1 << _FICD_JTAGEN_POSITION);		// JTAG is enabled

const uint32_t __attribute__((section (".SECTION_FPOR"))) temp_fpor =
	0xFFFFFFF0
	| (0b0 << _FPOR_LPBOREN_POSITION)		// Low-Power BOR is disabled
	| (0b0 << _FPOR_RETVR_POSITION)			// Retention regulator is enabled, controlled by RETEN in sleep
	| (0b00 << _FPOR_BOREN_POSITION);		// Brown-out reset disabled in HW, SBOREN bit disabled

const uint32_t __attribute__((section (".SECTION_FWDT"))) temp_fwdt =
	0xFFFF0000
	| (0b0 << _FWDT_FWDTEN_POSITION)		// Watchdog Timer disabled
	| (0b00 << _FWDT_RCLKSEL_POSITION)		// Clock source is system clock
	| (0b10000 << _FWDT_RWDTPS_POSITION)	// Run mode Watchdog Postscale is 1:65536
	| (0b1 << _FWDT_WINDIS_POSITION)		// Windowed mode is disabled
	| (0b00 << _FWDT_FWDTWINSZ_POSITION)	// Watchdog Timer window size is 75%
	| (0b10000 << _FWDT_SWDTPS_POSITION);	// Sleep mode Watchdog Postscale is 1:65536 

const uint32_t __attribute__((section (".SECTION_FOSCSEL"))) temp_foscsel =
	0xFFFF2828
	| (0b00 << _FOSCSEL_FCKSM_POSITION)		// Clock switching disabled, Fail-Safe clock monitor disabled
	| (0b1 << _FOSCSEL_SOSCSEL_POSITION)	// Crystal is used for SOSC, RA4/RB4 controlled by SOSC
	| (0b1 << _FOSCSEL_OSCIOFNC_POSITION)	// System clock not on CLKO pin, operates as normal I/O
	| (0b11 << _FOSCSEL_POSCMOD_POSITION)	// Primary oscillator is disabled
	| (0b0 << _FOSCSEL_IESO_POSITION)		// Two-speed Start-up is disabled
	| (0b0 << _FOSCSEL_SOSCEN_POSITION)		// Secondary oscillator disabled
	| (0b1 << _FOSCSEL_PLLSRC_POSITION)		// FRC is input to PLL on device Reset
	| (0b001 << _FOSCSEL_FNOSC_POSITION);	// Oscillator is (Primary or FRC) with PLL. -> By default, it's set to x3 in SPLLCON, so 24MHz total. Fix / implement later.

const uint32_t __attribute__((section (".SECTION_FSEC"))) temp_fsec =
	0x7FFFFFFF
	| (0b1 << _FSEC_CP_POSITION);			// Code protection disabled
// End PRIMARY configuration

// Configuration bits for the PIC32MM chip. ALTERNATE configuration
const uint32_t __attribute__((section (".SECTION_AFDEVOPT"))) temp_afdevopt = 
	0x0000FFF7
	| ((0xF0C5<<1) << _FDEVOPT_USERID_POSITION)	// UserID is F0C5. NOTE! for some reason, it's declared as _17_ bits wide in the .h file. 
												// Hence this hack, to have it aligned, since the definition shifts by 15, instead of 16...
	| (0b1 << _FDEVOPT_SOSCHP_POSITION);	// SOSC operated in Normal Power mode

const uint32_t __attribute__((section (".SECTION_AFICD"))) temp_aficd =
	0xFFFFFFE3
	| (0b01 << _FICD_ICS_POSITION)			// Communication is on PGEC3/PGED3
	| (0b1 << _FICD_JTAGEN_POSITION);		// JTAG is enabled

const uint32_t __attribute__((section (".SECTION_AFPOR"))) temp_afpor =
	0xFFFFFFF0
	| (0b0 << _FPOR_LPBOREN_POSITION)		// Low-Power BOR is disabled
	| (0b0 << _FPOR_RETVR_POSITION)			// Retention regulator is enabled, controlled by RETEN in sleep
	| (0b00 << _FPOR_BOREN_POSITION);		// Brown-out reset disabled in HW, SBOREN bit disabled

const uint32_t __attribute__((section (".SECTION_AFWDT"))) temp_afwdt =
	0xFFFF0000
	| (0b0 << _FWDT_FWDTEN_POSITION)		// Watchdog Timer disabled
	| (0b00 << _FWDT_RCLKSEL_POSITION)		// Clock source is system clock
	| (0b10000 << _FWDT_RWDTPS_POSITION)	// Run mode Watchdog Postscale is 1:65536
	| (0b1 << _FWDT_WINDIS_POSITION)		// Windowed mode is disabled
	| (0b00 << _FWDT_FWDTWINSZ_POSITION)	// Watchdog Timer window size is 75%
	| (0b10000 << _FWDT_SWDTPS_POSITION);	// Sleep mode Watchdog Postscale is 1:65536 

const uint32_t __attribute__((section (".SECTION_AFOSCSEL"))) temp_afoscsel =
	0xFFFF2828
	| (0b00 << _FOSCSEL_FCKSM_POSITION)		// Clock switching disabled, Fail-Safe clock monitor disabled
	| (0b1 << _FOSCSEL_SOSCSEL_POSITION)	// Crystal is used for SOSC, RA4/RB4 controlled by SOSC
	| (0b1 << _FOSCSEL_OSCIOFNC_POSITION)	// System clock not on CLKO pin, operates as normal I/O
	| (0b11 << _FOSCSEL_POSCMOD_POSITION)	// Primary oscillator is disabled
	| (0b0 << _FOSCSEL_IESO_POSITION)		// Two-speed Start-up is disabled
	| (0b0 << _FOSCSEL_SOSCEN_POSITION)		// Secondary oscillator disabled
	| (0b1 << _FOSCSEL_PLLSRC_POSITION)		// FRC is input to PLL on device Reset
	| (0b001 << _FOSCSEL_FNOSC_POSITION);	// Oscillator is (Primary or FRC) with PLL. -> By default, it's set to x3 in SPLLCON, so 24MHz total. Fix / implement later.

const uint32_t __attribute__((section (".SECTION_AFSEC"))) temp_afsec =
	0x7FFFFFFF
	| (0b1 << _FSEC_CP_POSITION);			// Code protection disabled
// End ALTERNATE configuration

volatile char tempArray[128];
volatile uint8_t lengthArray = 0;
uint32_t counter = 0;
uint8_t temp = 0;
uint32_t tempTwo = 0;

// TODO - run the timer, like in MX440 example (SysTick style)
void simpleDelay(unsigned int noOfLoops){
    unsigned int i = 0;
    while (i<noOfLoops){
        i++;
        asm("nop");
    }
}

void setup(){
	// What is the equivalent of SYSTEMConfigPerformance?
	// -> Setting up the system for the required System Clock
	// -> Seting up the Wait States
	// -> Setting up PBCLK
	// -> Setting up Cache module (not presenf on MX1/2, but is on MX4)
	// Also of interest: https://microchipdeveloper.com/32bit:mx-arch-exceptions-processor-initialization
	// See Pic32 reference manual, for CP0 info http://ww1.microchip.com/downloads/en/devicedoc/61113e.pdf

	// DO NOT setup KSEG0 (cacheable are) on MX1/MX2, debugging will NOT work

	//BMXCONbits.BMXWSDRM = 0;	// Set wait-states to 0
	
	// System config, call with desired CPU freq. and PBCLK divisor
	//SystemConfig(48000000L, 1);	// Set to 48MHz, with PBCLK with divider 1 (same settings as DEVCFG)

	//UARTDrv_Init(115200);

	// Enable interrupts
	//INTEnableSystemMultiVectoredInt();
}

void MIPS32 INTEnableSystemMultiVectoredInt(void)
{
    uint32_t val;

    // set the CP0 cause IV bit high
    asm volatile("mfc0   %0,$13" : "=r"(val));
    val |= 0x00800000;
    asm volatile("mtc0   %0,$13" : "+r"(val));

    INTCONSET = _INTCON_MVEC_MASK;

    // set the CP0 status IE bit high to turn on interrupts
    //INTEnableInterrupts();
	asm("ei");
}


void _general_exception_handler(){
	asm("nop");
	asm("nop");
	asm("nop");
	for(;;);
}


int main(){


//	setup();

//	tempArray[1] = 0;
	tempArray[0] = 'H';
	tempArray[1] = 'e';
	tempArray[2] = 'l';
	tempArray[3] = 'l';
	tempArray[4] = 'o';
	tempArray[5] = '\n';

	GPIODrv_Init();
	UARTDrv_Init(115200);

	INTCONbits.MVEC=1; 
	INTEnableSystemMultiVectoredInt();

    for(;;){
		tempTwo = UARTDrv_GetCount();
		tempArray[6] = (uint8_t)((tempTwo>>24) & 0xFF);
		tempArray[7] = (uint8_t)((tempTwo>>16) & 0xFF);
		tempArray[8] = (uint8_t)((tempTwo>>8) & 0xFF);
		tempArray[9] = (uint8_t)(tempTwo & 0xFF);
		
		UARTDrv_SendBlocking(tempArray, 10);
//		lengthArray = snprintf(tempArray, 128, "Hello from PIC32MM! %d\n", 5);
//		UARTDrv_SendBlocking(tempArray, lengthArray);


		if ((tempTwo & 0x03) == 0){
			LEDred_setState(1);
			LEDgreen_setState(1);
			LEDblue_setState(1);
		}
		else if ((tempTwo & 0x03) == 1){
			LEDred_setState(1);
			LEDgreen_setState(0);
			LEDblue_setState(0);
		}
		else if ((tempTwo & 0x03) == 2){
			LEDred_setState(0);
			LEDgreen_setState(1);
			LEDblue_setState(0);
		}
		else if ((tempTwo & 0x03) == 3){
			LEDred_setState(0);
			LEDgreen_setState(0);
			LEDblue_setState(1);
		}

		simpleDelay(500000);
    }

/**********************************************************************/

    return(0);
    
} // end of main


unsigned long __stack_chk_guard;
void __stack_chk_guard_setup(void){
     __stack_chk_guard = 0xBAAAAAAD;	// Provide some magic numbers
}

void * sbrk(uint32_t extraMem){

	extern uint8_t _heap_head;	// In linker
	extern uint8_t _heap_end;	// Also in linker

	static uint8_t * heap_head = 0;
	uint8_t * old_heap_head;



	if (heap_head == 0){
		heap_head = &_heap_head;
	}

	old_heap_head = heap_head;

	if ((heap_head + extraMem) > &_heap_end){
		// FYI, code execution will be blocked if you get here.
		errno = ENOMEM;
		return (void *) -1;
	}

	heap_head = heap_head + extraMem;
	return (void *) old_heap_head;
}

