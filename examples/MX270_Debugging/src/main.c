#include <p32xxxx.h>    // Always in first place to avoid conflict with const.h -> this includes xc.h
#include <system.h>     // System setup
#include <const.h>      // MIPS32
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <newlib.h>
#include <errno.h>

// Drivers for HW
#include <GPIODrv.h>
#include <UARTDrv.h>
#include <BTN.h>
#include <LED.h>

const uint32_t __attribute__((section (".SECTION_DEVCFG3"))) temp3 = 
	0xFFF0000
	| (0b1 << _DEVCFG3_FVBUSONIO_POSITION)	// USBVBUSON controlled by USB module
	| (0b0 << _DEVCFG3_FUSBIDIO_POSITION)	// USBID controlled by PORT function
	| (0b0 << _DEVCFG3_IOL1WAY_POSITION)		// Allow multiple reconfigurations of Peripheral Pins
	| (0b0 << _DEVCFG3_PMDL1WAY_POSITION)	// Allow multiple reconfigurations of Peripheral Module Disable
	| (0xF0C5 << _DEVCFG3_USERID_POSITION);	// UserID is F0C5

const uint32_t __attribute__((section (".SECTION_DEVCFG2"))) temp2 =
	0xFFF87888
	| (0b001 << _DEVCFG2_FPLLODIV_POSITION)	// PLL output divided by 2 (96MHz/2 = 48Mhz)
	| (0b0 << _DEVCFG2_UPLLEN_POSITION)		// USB PLL Enabled
	| (0b001 << _DEVCFG2_UPLLIDIV_POSITION)	// USB PLL input divided by 2 (8MHz/2 = 4MHz)
	| (0b111 << _DEVCFG2_FPLLMUL_POSITION)	// PLL Multiplier is 24 (4MHz*24 = 96MHz)
	| (0b001 << _DEVCFG2_FPLLIDIV_POSITION);	// PLL input divided by 2 (8MHz/2 = 4MHz)

const uint32_t __attribute__((section (".SECTION_DEVCFG1"))) temp1 =
	0xFC200858
	| (0b00 << _DEVCFG1_FWDTWINSZ_POSITION)	// Watchdog timer window size is 75%
	| (0b0 << _DEVCFG1_FWDTEN_POSITION)		// Watchdog timer disabled, can be enabled in software
	| (0b00000 << _DEVCFG1_WDTPS_POSITION)	// Watchdog timer postscale is 1
	| (0b01 << _DEVCFG1_FCKSM_POSITION)		// Clock switching enabled, Fail-Safe Clock Monitoring DISABLED
	| (0b00 << _DEVCFG1_FPBDIV_POSITION)	// PBCLK is SYSCLK / 1
	| (0b1 << _DEVCFG1_OSCIOFNC_POSITION)	// CLOCK output disabled
	| (0b01 << _DEVCFG1_POSCMOD_POSITION)	// XT ocillator mode
	| (0b0 << _DEVCFG1_IESO_POSITION)		// Internal-External switchover disabled (Two-speed start-up disabled)
	| (0b1 << _DEVCFG1_FSOSCEN_POSITION)		// Enable secondary oscillator (WARNING; CHECK IF PORTING)
	| (0b011 << _DEVCFG1_FNOSC_POSITION);		// POSC (XT) + PLL selected

const uint32_t __attribute__((section (".SECTION_DEVCFG0"))) temp0 =
	0x6EF803E0								// Don't forget about that one 0
	| (0b1 << _DEVCFG0_CP_POSITION)			// Code Protection disabled
	| (0b1 << _DEVCFG0_BWP_POSITION)			// Boot Flash is Writeable during code execution
	| (0b1111111111 << _DEVCFG0_PWP_POSITION)	// Memory is NOT write-protected
	| (0b00 << _DEVCFG0_ICESEL_POSITION)		// PGEC4/PGED4 is used
	| (0b1 << _DEVCFG0_JTAGEN_POSITION)		// JTAG is enabled
#ifdef DEBUG_BUILD							// Defined with Makefile
	| (0b11<<_DEVCFG0_DEBUG_POSITION);		// Debugger is DISABLED. Apparently the MX1/MX2 family need this disabled, for JTAG to work
											// Note, application will run automatically, might want to add a delay at the beginning.
#else
	| (0b11<<_DEVCFG0_DEBUG_POSITION);		// Debugger is DISABLED (DEBUG bit) - DEBUG NEEDS TO BE DISABLED, IF NO DEBUGGER PRESENT! Otherwise code doesn't run.
#endif

volatile char tempArray[128];
volatile uint8_t lengthArray = 0;

// TODO - run the timer, like in MX440 example (SysTick style)
void simpleDelay(unsigned int noOfLoops){
    unsigned int i = 0;
    while (i<noOfLoops){
        i++;
        asm("nop");
    }
}

void setup(){
	// What is the equivalent of SYSTEMConfigPerformance?
	// -> Setting up the system for the required System Clock
	// -> Seting up the Wait States
	// -> Setting up PBCLK
	// -> Setting up Cache module (not presenf on MX1/2, but is on MX4)
	// Also of interest: https://microchipdeveloper.com/32bit:mx-arch-exceptions-processor-initialization
	// See Pic32 reference manual, for CP0 info http://ww1.microchip.com/downloads/en/devicedoc/61113e.pdf

	// DO NOT setup KSEG0 (cacheable are) on MX1/MX2, debugging will NOT work

	BMXCONbits.BMXWSDRM = 0;	// Set wait-states to 0
	
	// System config, call with desired CPU freq. and PBCLK divisor
	SystemConfig(48000000L, 1);	// Set to 48MHz, with PBCLK with divider 1 (same settings as DEVCFG)

	//UARTDrv_Init(115200);

	LED_init();
	BTN_init();
	UARTDrv_Init(115200);

	// Enable interrupts
	INTEnableSystemMultiVectoredInt();
}

int main(){


	setup();

	tempArray[1] = 0;

    for(;;){
    	if (BTN_getStatus()){
    		LED_setState(1);
    		tempArray[0] = 0xaa;
    		tempArray[1] = UARTDrv_GetCount();
    	}
    	else{
    		LED_setState(0);
    		tempArray[0] = 0x55;
    		tempArray[1] = UARTDrv_GetCount();
    	}
    	UARTDrv_SendBlocking((uint8_t *) tempArray, 2);
    }

/**********************************************************************/

    return(0);
    
} // end of main


