#!/usr/bin/env python3

import os
import re

def fix_file(filepath):
    path, filename = os.path.split(filepath)
    with open(os.path.join(path, filename)) as inp:
        contents = inp.read()

    fixes = []

    def fix_line(l):
        stripped = l.strip()
        if not stripped.startswith('.section'):
            return l
        finds = re.findall('^.section *([^ ]+?), *(.+)$', stripped)
        new_params = 'wa'
        if finds:
            name, paramstr = finds[0]
            params = set(p.strip() for p in paramstr.split(','))
            if any('address' in p for p in params):
                new_params = new_params + 'd'
        else:
            name = re.findall('^.section *([^ ]+?)$', stripped)[0][0]
            params = set()
        fixes.append(name, params)
        return '    .section {},"{}"'.format(name,new_params)

    fixed_contents = '\n'.join(fix_line(line) for line in contents.splitlines())
    if fixed_contents.strip() == contents.strip():
        return None
    basename, ext = os.path.splitext(filename)
    fixname = "{}_fix.S".format(basename)
    with open(os.path.join(path, fixname), 'w') as out:
        out.write(fixed_contents)

    return fixname, fixes

def make_linker_script(filepath, fixes):
    with open(filepath, 'w') as f:
        for name, fixs in fixes:
            

if __name__ == '__main__':
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("path", nargs="?")
    args = parser.parse_args()

    if args.path:
        fix_file(args.path)
    else:
        for path, dirnames, filenames in os.walk('.'):
            for filename in filter(lambda f: f.endswith('.S') and not f.endswith('_fix.S'), filenames):
                name, fixes = fix_file(os.path.join(path, filename))
                make_linker_script(path + '.ld', fixes)
