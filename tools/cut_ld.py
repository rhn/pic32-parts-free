#!/usr/bin/python3

"""This file splits part-specific linker file into procdefs and the general part.
Applies to pic32mx files provided by Microchip as of now.
"""

from glob import glob
import os


def split_script(linker, procdefs):
    license_end = linker.index('*/')
    code_start = linker.index('OUTPUT_FORMAT')
    procdefs_start = linker.index(procdefs)
    procdefs_end = procdefs_start + len(procdefs)
    
    license = linker[:license_end]
    help_message = linker[license_end:code_start]
    linker_head = linker[code_start:procdefs_start]
    # procdefs already known
    linker_body = linker[procdefs_end:]
    
    return "\n".join([license, linker_head, linker_body]), "\n".join([license, help_message, procdefs])

def update_directory(linker, procdefs, path):
    with open(os.path.join(path, 'procdefs.ld'), 'w') as f:
        f.write(procdefs)
    with open(os.path.join(path, 'general.ld'), 'w') as f:
        f.write(linker)

def process_directory(path):
    procdefs_path = os.path.join(path, 'procdefs.ld')
    with open(procdefs_path) as f:
        procdefs = f.read()
    with open(set(glob(os.path.join(path, '*.ld'))).difference([procdefs_path]).pop()) as f:
        linker = f.read()
    
    new_linker, new_procdefs = split_script(linker, procdefs)
    update_directory(new_linker, new_procdefs, path)

if __name__ == "__main__":
    import sys
    for arg in sys.argv[1:]:
        process_directory(arg)
