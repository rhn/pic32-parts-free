/*  --------------------------------------------------------------------
    FILE:               isrwrapper.c
    PROJECT:            pic32-parts-free
    PURPOSE:            weak definition of isr routines
    PROGRAMERS:         Regis Blanchot <rblanchot@gmail.com>
                        Primož "Neofoxx" Kranjec <kranjec.primoz@gmail.com>
    FIRST RELEASE:      05 Feb. 2015
    LAST RELEASE:       25 Jan. 2020
    --------------------------------------------------------------------
    CHANGELOG:
        20200125: Neofoxx:	
            Added all functions that can appear in MX&MM families
            Added the General Exception handler
    --------------------------------------------------------------------
    NOTE: Some vectors on PIC32MX5/6/7 are shared. Vectors were added.

    --------------------------------------------------------------------
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
----------------------------------------------------------------------*/

#ifndef ISRWRAPPER_C
#define ISRWRAPPER_C

// Generally, in DoNothing, there should be an infinite loop,
// to help with debug, if interrupt is undefined.
void __attribute__ ((interrupt ))__DoNothing() {/* do something */ };

void __attribute__ ((interrupt ))__DoNothingGenException(){
	// In your handler, decode exception, then reset.
	// In this one, wait forever
	for(;;);
}

// The functions below don't need to have "interrupt" in the attributes,
// as the aliased function already has it.

// To define your own function elsewhere, define it with:
// ISR(functionName){}
// The ISR mmcro will expand into void __attribute__ ((interrupt)) functionName()

// CORE INTERRUPTS
void CoreTimerInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void CoreSoftware0Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void CoreSoftware1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM

// EXTERNAL INTERRUPTS
void External0Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void External1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void External2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void External3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void External4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM

// CHANGE NOTICE INTERRUPTS
void ChangeNoticeAInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MM 
void ChangeNoticeBInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MM
void ChangeNoticeCInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MM
void ChangeNoticeDInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MM
void ChangeNoticeInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

// TIMER INTERRUPTS
void Timer1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void Timer2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void Timer3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void Timer4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void Timer5Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

// COMPARATOR INTERRUPTS
void Comparator1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void Comparator2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void Comparator3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX1/2xx, PIC32MM

// RTCC INTERRUPTS
void RTCCInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM

// ADC INTERRUPTS
void ADCInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM

// CRC INTERRUPTS
void CRCInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM

// HLVD INTERRUPT
void HLVDInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM

// CLC INTERRUPTS
void CLC1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CLC2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CLC3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CLC4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM

// SPI INTERRUPTS
void SPI1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void SPI2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx
void SPI3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx
void SPI4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx

void SPI1ErrorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void SPI1TxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void SPI1RxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void SPI2ErrorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void SPI2TxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void SPI2RxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void SPI3ErrorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void SPI3TxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void SPI3RxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM

// UART INTERRUPTS
void UART1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx
void UART2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx
void UART3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, IC32MX3/4xx
void UART4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx
void UART5Interrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MX1/2xx, PIC32MX3/4xx

void UART1RxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void UART1TxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void UART1ErrorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void UART2RxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void UART2TxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void UART2ErrorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void UART3RxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void UART3TxInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void UART3ErrorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
// -> ADD SHARED INTERRUPTS!!


// I2C INTERRUPTS
void I2C1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void I2C2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

void I2C1SlaveInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void I2C1MasterInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void I2C1BusInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM
void I2C2SlaveInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void I2C2MasterInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MM
void I2C2BusInterrupt() __attribute__ ((weak, alias ("__DoNothing")));			// PIC32MM

// UART/SPI/I2C SHARED INTERRUPTS
void I2C3_SPI3_UART1_SharedInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX5/6/7xx
void I2C4_SPI2_UART3_SharedInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX5/6/7xx
void I2C5_SPI4_UART2_SharedInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX5/6/7xx
void UART4_UART1B_SharedInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX5/6/7xx
void UART6_UART2B_SharedInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX5/6/7xx
void UART5_UART3B_SharedInterrupt() __attribute__ ((weak, alias ("__DoNothing")));		// PIC32MX5/6/7xx



// INPUT CAPTURE / OUTPUT COMPARE INTERRUPTS
void InputCapture1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void InputCapture2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void InputCapture3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void InputCapture4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void InputCapture5Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

void OutputCapture1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void OutputCapture2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void OutputCapture3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void OutputCapture4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void OutputCapture5Interrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

void CCP1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP5Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP6Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP7Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP8Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCP9Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM

// Subset - CCP TIMER INTERRUPTS
void CCT1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT5Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT6Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT7Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT8Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void CCT9Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM

// FRC TUNE and DCO TUNE INTERRUPTS
void FRCTuneInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void DCOTuneInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM

// NVM or FCE INTERRUPT
void NVMInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM
void FCEInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

// PERFORMANCE COUNTER INTERRUPT
void PerformanceCounterInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MM

// ECC Single Bit INTERRUPT
void ECCSingleBitErrorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MM

// FAILSAFE MONITOR INTERRUPT
void FailSafeMonitorInterrupt() __attribute__ ((weak, alias ("__DoNothing")));	// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

// USB INTERRUPT
void USB1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx
void USBInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MM

// PMP INTERRUPT
void PMPInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx

// CTMU INTERRUPT
void CTMUInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx

// DMA INTERRUPT
void DMA0Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void DMA1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void DMA2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void DMA3Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX3/4xx, PIC32MX5/6/7xx, PIC32MM
void DMA4Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX5/6/7xx
void DMA5Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX5/6/7xx
void DMA6Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX5/6/7xx
void DMA7Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX5/6/7xx

// CAN INTERRUPT
void CAN1Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX1/2xx, PIC32MX5/6/7xx
void CAN2Interrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX5/6/7xx

// ETH INTERRUPT
void ETHInterrupt() __attribute__ ((weak, alias ("__DoNothing")));				// PIC32MX5/6/7xx

// General exception handler. Defined in general_exception.S
void _general_exception_handler() __attribute__ ((weak, alias ("__DoNothingGenException")));


#endif // ISRWRAPPER_C

