#ifndef INTERRUPT_H_c0d1278cf1e449b79fb3300a1fe12f51
#define INTERRUPT_H_c0d1278cf1e449b79fb3300a1fe12f51

/* Modeled after avr-libc
** Use as INTERRUPT(vectorFunctionName, optionalArguments){code;}
** For arguments, see gcc mips attributes 
** -> https://gcc.gnu.org/onlinedocs/gcc/MIPS-Function-Attributes.html
*/
#define INTERRUPT(vector, ...)            \
	void vector (void) __attribute__ ((interrupt, __VA_ARGS__)); \
	void vector (void)

#endif
